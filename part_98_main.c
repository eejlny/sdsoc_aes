/*Copyright (c) 2015, Adam Taylor
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "sds_lib.h"
#include "part_98_aes_enc.h"
#include "part_98_sbox.h"

//#define block_size 1024000 //size of the block to process

uint8_t key[32] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f};  // initial key



uint8_t state_temp[32] =
{0xFF,0x44,0x88,0xcc,0x11,0x55,0x99,0xdd,0x22,0x66,0xaa,0xee,0x33,0x77,0xbb,0xff,
0xFF,0x44,0x88,0xcc,0x11,0x55,0x99,0xdd,0x22,0x66,0xaa,0xee,0x33,0x77,0xbb,0xff};

#define TIME_STAMP_INIT_SW  unsigned long long clock_start_sw, clock_end_sw;  clock_start_sw = sds_clock_counter();
#define TIME_STAMP_SW  { clock_end_sw = sds_clock_counter(); printf("Average number of processor cycles : %llu \n", (clock_end_sw-clock_start_sw));   }

#define TIME_STAMP_INIT_HW  unsigned long long clock_start, clock_end;  clock_start = sds_clock_counter();
#define TIME_STAMP_HW  { clock_end = sds_clock_counter(); printf("Average number of processor cycles : %llu \n", (clock_end-clock_start)); }

void read_input(uint32_t *state, char *file)
{
	FILE *fp;

	fp = fopen (file, "r");
	if (!fp)
	{
		printf("file could not be opened for reading\n");
		exit(1);
	}
	fread(state, group_size, 1, fp); // Read in the entire block
	fclose(fp);
}

void write_output(uint32_t *state, char *file)
{
	FILE *fp;

	fp = fopen (file, "w");
	if (!fp)
	{
		printf("file could not be opened for writing\n");
		exit(1);
	}
	fwrite(state, group_size, 1, fp); // Read in the entire block
	fclose(fp);
}

void keyexpansion(uint8_t key[32], uint8_t ekey[240]);

int main(int argc, char* argv[])
{
	int x,y,i;
	uint32_t *state;
	uint8_t *state_sw;
	uint32_t *cipher;
	uint8_t *cipher_sw;
	uint8_t vbyte[4];
	uint8_t sbyte[4];
	uint32_t  vinteger;

	char *ifile, *ofile;

	uint8_t *ekey;
	uint8_t *ekey_sw;
	ekey = (uint8_t *) sds_alloc(240 *sizeof(uint8_t));
	ekey_sw = (uint8_t *) sds_alloc(240 *sizeof(uint8_t));
	state = (uint32_t*) sds_alloc(int_group_size * sizeof(uint32_t));
	state_sw = (uint8_t*) sds_alloc(group_size * sizeof(uint8_t));
	cipher = (uint32_t*) sds_alloc(int_group_size * sizeof(uint32_t));
	cipher_sw = (uint8_t*) sds_alloc(group_size*sizeof(uint8_t));


	keyexpansion(key,ekey);
	keyexpansion(key,ekey_sw);

	if (argc != 3)
		printf("incorrect number of insputs: aes in_file out_file\n");

	ifile = argv[1];
	ofile = argv[2];

  #if DEBUG==1
	for (i=0;i<group_size;i+=4)
	{
		*(state+i/4) = (state_temp[i+3] << 24) | (state_temp[i+2] << 16 ) | (state_temp[i+1] << 8) | state_temp[i];
		*(state_sw+i) = state_temp[i];
		*(state_sw+i+1) = state_temp[i+1];
		*(state_sw+i+2) = state_temp[i+2];
		*(state_sw+i+3) = state_temp[i+3];
	}

  #else
   	 read_input(state, ifile);
 	 read_input(state_sw, ifile);
  #endif

	printf("Running in software\n");

	TIME_STAMP_INIT_SW

	aes_enc(state_sw,cipher_sw,ekey_sw);

	TIME_STAMP_SW

	printf("Running in hardware\n");
	TIME_STAMP_INIT_HW


	aes_enc_async(state,cipher,ekey);

	TIME_STAMP_HW

	#if DEBUG==1
	#else
		write_output(cipher, ofile);
	#endif

	for(x=0;x<group_size;x+=4)
	{
		vbyte[3] = (*(cipher+x/4) >> 24) & 0xFF;
		vbyte[2] = (*(cipher+x/4) >> 16) & 0xFF;
		vbyte[1] = (*(cipher+x/4) >> 8) & 0xFF;
		vbyte[0] = *(cipher+x/4) & 0xFF;
		sbyte[0] = *(cipher_sw+x) & 0xFF;
		sbyte[1] = *(cipher_sw+x+1) & 0xFF;
		sbyte[2] = *(cipher_sw+x+2) & 0xFF;
		sbyte[3] = *(cipher_sw+x+3) & 0xFF;

		if ((vbyte[0] == sbyte[0]) && (vbyte[1] == sbyte[1]) && (vbyte[2] == sbyte[2]) && (vbyte[3] == sbyte[3]))
		{

		}
		else
		{
			printf("Results do not match at %d\n",x);
			printf("HW %x SW %x HW %x SW %x HW %x SW %x HW %x SW %x\n", vbyte[0],sbyte[0],vbyte[1],sbyte[1],vbyte[2],sbyte[2],vbyte[3],sbyte[3]);
			exit(1);
		}
	}
	printf("Results match !!! \n");
	printf("\n");
	return 0;
}

void keyexpansion(uint8_t key[32], uint8_t ekey[240])
{
	  uint32_t i, j, k;
	  uint8_t temp[4];

	  for(i = 0; i < nk; ++i)
	  {
	    ekey[(i * 4) + 0] = key[(i * 4) + 0];
	    ekey[(i * 4) + 1] = key[(i * 4) + 1];
	    ekey[(i * 4) + 2] = key[(i * 4) + 2];
	    ekey[(i * 4) + 3] = key[(i * 4) + 3];
	  }


	  for(; (i < (nb * (nr + 1))); ++i)
	  {
	    for(j = 0; j < 4; ++j)
	    {
	      temp[j]= ekey[(i-1) * 4 + j];
	    }
	    if (i % nk == 0)
	    {
	      {
	        k = temp[0];
	        temp[0] = temp[1];
	        temp[1] = temp[2];
	        temp[2] = temp[3];
	        temp[3] = k;
	      }


	      {
	        temp[0] = sbox[temp[0]];
	        temp[1] = sbox[temp[1]];
	        temp[2] = sbox[temp[2]];
	        temp[3] = sbox[temp[3]];
	      }

	      temp[0] =  temp[0] ^ Rcon[i/nk];
	    }
	    else if (nk > 6 && i % nk == 4)
	    {
	      // Function Subword()
	      {
	        temp[0] = sbox[temp[0]];
	        temp[1] = sbox[temp[1]];
	        temp[2] = sbox[temp[2]];
	        temp[3] = sbox[temp[3]];
	      }
	    }
	    ekey[i * 4 + 0] = ekey[(i - nk) * 4 + 0] ^ temp[0];
	    ekey[i * 4 + 1] = ekey[(i - nk) * 4 + 1] ^ temp[1];
	    ekey[i * 4 + 2] = ekey[(i - nk) * 4 + 2] ^ temp[2];
	    ekey[i * 4 + 3] = ekey[(i - nk) * 4 + 3] ^ temp[3];
	  }

}

