# README #

DEMO AES version for SDSOC with 2 cores (hardware threads working in parallel). This version reads 1 MB of data from input file and encrypts in software and then in hardware and then compares the results so it self-checks.

To implement create a Linux SDSOC project for a Zynq board (e.g tested in zc702) and add  aes_en_hw as your hardware function. Then implement.

There are several functions that are not used but left as reference. To change the number of bytes in the header. 

#define group_size 1048576   //the size of file in bytes
#define int_group_size 262144 //this divided /4