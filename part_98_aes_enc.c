/*Copyright (c) 2015, Adam Taylor
All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project*/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <sds_lib.h>
#include "part_98_sbox.h"
#include "part_98_aes_enc.h"



void aes_wrapper_df(uint8_t state[16],uint8_t cipher[16],uint8_t ekey[240]);
void aes_wrapper(uint8_t state[16],uint8_t cipher[16],uint8_t ekey[240],bool start, bool end);
void shift_row_enc(uint8_t state[16], uint8_t result[16]);
void subbytes(uint8_t state[16], uint8_t result[16]);
void addroundkey(uint8_t state[16],uint8_t iteration, uint8_t result[16],uint8_t ekey[240]);
void mixcolumn(uint8_t state[16], uint8_t result[16]);

void shift_row_enc2(uint8_t state[16], uint8_t result[16]);
void subbytes2(uint8_t state[16], uint8_t result[16]);
void addroundkey2(uint8_t state[16],uint8_t iteration, uint8_t result[16],uint8_t ekey[240]);
void mixcolumn2(uint8_t state[16], uint8_t result[16]);

void shift_row_enc3(uint8_t state[16], uint8_t result[16]);
void subbytes3(uint8_t state[16], uint8_t result[16]);
void addroundkey3(uint8_t state[16],uint8_t iteration, uint8_t result[16],uint8_t ekey[240]);
void mixcolumn3(uint8_t state[16], uint8_t result[16]);

void shift_row_enc4(uint8_t state[16], uint8_t result[16]);
void subbytes4(uint8_t state[16], uint8_t result[16]);
void addroundkey4(uint8_t state[16],uint8_t iteration, uint8_t result[16],uint8_t ekey[240]);
void mixcolumn4(uint8_t state[16], uint8_t result[16], bool end);

void shift_row_enc5(uint8_t state[16], uint8_t result[16]);
void subbytes5(uint8_t state[16], uint8_t result[16]);
void addroundkey5(uint8_t state[16],uint8_t iteration, uint8_t result[16],uint8_t ekey[240]);
void mixcolumn5(uint8_t state[16], uint8_t result[16]);

void shift_row_enc6(uint8_t state[16], uint8_t result[16]);
void subbytes6(uint8_t state[16], uint8_t result[16]);
void addroundkey6(uint8_t state[16],uint8_t iteration, uint8_t result[16],uint8_t ekey[240]);
void mixcolumn6(uint8_t state[16], uint8_t result[16]);




uint8_t extractbyte(uint32_t src, unsigned from, unsigned to)
{
  unsigned mask = ( (1<<(to-from+1))-1) << from;
  return (src & mask) >> from;
}


#pragma SDS data copy(state[0:int_group_size/2])
#pragma SDS data copy(cipher[0:int_group_size/2])
#pragma SDS data access_pattern(state:SEQUENTIAL)
#pragma SDS data access_pattern(cipher:SEQUENTIAL)
#pragma SDS data data_mover(state:AXIDMA_SIMPLE)
#pragma SDS data data_mover(cipher:AXIDMA_SIMPLE)
#pragma SDS data mem_attribute(state: CACHEABLE)
#pragma SDS data mem_attribute(cipher: CACHEABLE)
#pragma SDS data mem_attribute(ekey: CACHEABLE)
//#pragma SDS data sys_port(state:ACP)
//#pragma SDS data sys_port(cipher:ACP)
void aes_enc_hw(uint32_t *state,uint32_t *cipher,uint8_t ekey[240])

{

	int i;
	uint8_t iteration = 0;
	uint8_t x,y;
	int z,j;
	uint8_t sub[16];
	uint8_t shift[16];
	uint8_t mix[16];
	uint8_t round[16];
	uint8_t state_grid[16];
	uint8_t result[16];

for(i=0;i<int_group_size/2;i+=4)
{

	//#pragma HLS PIPELINE //(IL 323 clock cycles)
			for(j=0; j<4; j++) {
			       #pragma HLS PIPELINE
		        		 state_grid[4*j] = extractbyte(*(state+j+i), 0, 7);
			             state_grid[4*j+1] = extractbyte(*(state+j+i), 8, 15);
			             state_grid[4*j+2] = extractbyte(*(state+j+i), 16, 23);
			             state_grid[4*j+3] = extractbyte(*(state+j+i), 24, 31);
		                 //state_grid[j] = *(state+j+i);
			}

	addroundkey(state_grid,0,sub,ekey);
	loop_main : for(iteration = 1; iteration < nr; iteration++)
	 {

	   subbytes(sub,shift);
	   shift_row_enc(shift,mix);
	   mixcolumn(mix,round);
	   addroundkey(round,iteration,sub,ekey);
	  }
	  subbytes(sub,shift);
	  shift_row_enc(shift,round);
	  addroundkey(round,nr,result,ekey);

	  for(z=0; z<4; z++) {
		    	 #pragma HLS PIPELINE
		    	   //*(cipher+z+i) = result[z];
		    	   *(cipher+z+i) = (result[4*z+3] << 24) | (result[4*z+2] << 16 ) | (result[4*z+1] << 8) | result[4*z];
	  }
  }
}
/*
#pragma SDS data copy(state_full[0:int_group_size])
#pragma SDS data copy(cipher_full[0:int_group_size])
#pragma SDS data access_pattern(state_full:SEQUENTIAL)
#pragma SDS data access_pattern(cipher_full:SEQUENTIAL)
#pragma SDS data data_mover(state_full:AXIDMA_SIMPLE)
#pragma SDS data data_mover(cipher_full:AXIDMA_SIMPLE)
*/
void aes_enc_async(uint32_t *state_full,uint32_t *cipher_full,uint8_t ekey[240])
{
	uint32_t *state_temp = state_full;
	uint32_t *cipher_temp = cipher_full;

	#pragma SDS async(1)
	aes_enc_hw(state_temp,cipher_temp,ekey);
	state_temp = state_full + int_group_size/2;
	cipher_temp = cipher_full + int_group_size/2;
	#pragma SDS async(2)
	aes_enc_hw(state_temp,cipher_temp,ekey);
	#pragma SDS wait(1)
	#pragma SDS wait(2)
}

/*
#pragma SDS data copy(state[0:group_size])
#pragma SDS data copy(cipher[0:group_size])
#pragma SDS data access_pattern(state:SEQUENTIAL)
#pragma SDS data access_pattern(cipher:SEQUENTIAL)
#pragma SDS data data_mover(state:AXIDMA_SIMPLE)
#pragma SDS data data_mover(cipher:AXIDMA_SIMPLE)
*/

/*
#pragma SDS data copy(state[0:group_size])
#pragma SDS data copy(cipher[0:group_size])
#pragma SDS data access_pattern(state:SEQUENTIAL)
#pragma SDS data access_pattern(cipher:SEQUENTIAL)
#pragma SDS data data_mover(state:AXIDMA_SIMPLE)
#pragma SDS data data_mover(cipher:AXIDMA_SIMPLE)
*/

void aes_enc(uint8_t *state,uint8_t *cipher,uint8_t ekey_buf[240])

{

	int i,j,z,y,v;
	uint8_t iteration = 0;
	uint8_t state_buf[16];
	uint8_t cipher_buf[16];
	//uint8_t ekey_buf[240];
    #pragma HLS array_partition variable=state_buf complete
    #pragma HLS array_partition variable=cipher_buf complete
    //#pragma HLS array_partition variable=state_buf complete
    //#pragma HLS array_partition variable=cipher_buf complete
	//#pragma HLS array_partition variable=state_buf complete
	//#pragma HLS array_partition variable=cipher_buf complete
   // #pragma HLS array_partition variable=ekey_buf complete
/*	uint8_t sub_0[16];
	uint8_t sub_1[16];
	uint8_t shift_0[16];
	uint8_t shift_1[16];
	uint8_t mix_0[16];
	uint8_t mix_1[16];
	uint8_t round_0[16];
	uint8_t round_1[16];*/
//	uint8_t state_grid[4][4];
//	uint8_t result[4][4];

//#pragma HLS DATAFLOW

/*for(j=0; j<240; j++) {
	#pragma HLS PIPELINE
      ekey_buf[j] = *(ekey+j);
}*/

//for(i=0;i<block_size;i+=group_size)
//{
     //#pragma HLS DATAFLOW
	 loop_y : for(y=0;y<group_size;y+=16)
	 {
		//#pragma HLS PIPELINE //(IL 323 clock cycles)
		for(j=0; j<16; j++) {
		       #pragma HLS PIPELINE
	             state_buf[j] = *(state+j+y);
		}
		aes_wrapper_df(state_buf,cipher_buf,ekey_buf);
		for(z=0; z<16; z++) {
		    	 #pragma HLS PIPELINE
		    	   *(cipher+z+y) = cipher_buf[z];
	    }
     }
//}
//#pragma AP PIPELINE II = 1
	/*state_grid[0][0] = state[i+0];
	state_grid[0][1] = state[i+1];
	state_grid[0][2] = state[i+2];
	state_grid[0][3] = state[i+3];
	state_grid[1][0] = state[i+4];
	state_grid[1][1] = state[i+5];
	state_grid[1][2] = state[i+6];
	state_grid[1][3] = state[i+7];
	state_grid[2][0] = state[i+8];
	state_grid[2][1] = state[i+9];
	state_grid[2][2] = state[i+10];
	state_grid[2][3] = state[i+11];
	state_grid[3][0] = state[i+12];
	state_grid[3][1] = state[i+13];
	state_grid[3][2] = state[i+14];
	state_grid[3][3] = state[i+15];*/

	//memcpy(state_grid,(state+i),16*sizeof(uint8_t));
	//memcpy(state_grid,(state),16*sizeof(uint8_t));

	/*for(i=0;i<4;i++)
		for(j=0;j<4;j++)
			result[i][j]=state_grid[i][j]+1;*/

/*
	addroundkey((state+i),0,sub_0,ekey);
	subbytes(sub_0,shift_0);
	shift_row_enc(shift_0,mix_0);
	mixcolumn(mix_0,round_0);
	addroundkey2(round_0,1,(cipher+i),ekey);*/

/*
    //#pragma HLS DATAFLOW
	//loop_main : for(iteration = 1; iteration < nr; iteration++)
/*	iteration = 1;
	//{

	   subbytes(sub_0,shift_0);
	   shift_row_enc(shift_0,mix_0);
	   mixcolumn(mix_0,round_0);
//	   addroundkey(round,iteration,(cipher+i),ekey);
	   addroundkey2(round_0,iteration,sub_1,ekey);
	  //}
	  subbytes2(sub_1,shift_1);
	  shift_row_enc2(shift_1,round_1);
	  addroundkey3(round_1,nr,(cipher+i),ekey);
	  //addroundkey(round,nr,result,ekey);

//	  memcpy((cipher),result,16*sizeof(uint8_t));
	  //memcpy((cipher+i),result,16*sizeof(uint8_t));


	 /* cipher[i+0] = result[0][0];
	  cipher[i+1] = result[0][1];
	  cipher[i+2] = result[0][2];
	  cipher[i+3] = result[0][3];
	  cipher[i+4] = result[1][0];
	  cipher[i+5] = result[1][1];
	  cipher[i+6] = result[1][2];
	  cipher[i+7] = result[1][3];
	  cipher[i+8] = result[2][0];
	  cipher[i+9] = result[2][1];
	  cipher[i+10] = result[2][2];
	  cipher[i+11] = result[2][3];
	  cipher[i+12] = result[3][0];
	  cipher[i+13] = result[3][1];
	  cipher[i+14] = result[3][2];
	  cipher[i+15] = result[3][3];*/
  //}
}

void aes_wrapper(uint8_t state[16],uint8_t cipher[16],uint8_t ekey[240],bool start,bool end)
{
	uint8_t loop;
	uint8_t sub_0[16];
	uint8_t sub_1[16];
	uint8_t sub_2[16];
	uint8_t sub_3[16];
	uint8_t sub_4[16];
	uint8_t shift_0[16];
	uint8_t shift_1[16];
	uint8_t shift_2[16];
	uint8_t shift_3[16];
	uint8_t shift_4[16];
	uint8_t mix_0[16];
	uint8_t mix_1[16];
	uint8_t mix_2[16];
	uint8_t mix_3[16];
	uint8_t round_0[16];
	uint8_t round_1[16];
	uint8_t round_2[16];
	uint8_t round_3[16];
	uint8_t round_4[16];
	//#pragma HLS DATAFLOW



	addroundkey(state,0,sub_0,ekey);

	//addroundkey(state,0,cipher,ekey,start);

	for (loop =0;loop < nr; loop++)
	{
		subbytes(sub_0,shift_0);
		shift_row_enc(shift_0,mix_0);
		mixcolumn(mix_0,round_0);
		addroundkey2(round_0,1,sub_0,ekey);
	}

/*	subbytes2(sub_1,shift_1);
	shift_row_enc2(shift_1,mix_1);
	mixcolumn2(mix_1,round_1);
	addroundkey3(round_1,2,sub_2,ekey);
*/
	subbytes5(sub_0,shift_3);
	shift_row_enc5(shift_3,round_3);
	addroundkey6(round_3,4,cipher,ekey);

}


void aes_wrapper_df(uint8_t state[16],uint8_t cipher[16],uint8_t ekey[240])
{
	uint8_t loop1,loop2,loop3;
	uint8_t sub_0[16];
	uint8_t sub_1[16];
	uint8_t sub_2[16];
	uint8_t sub_3[16];
	uint8_t sub_4[16];
	uint8_t shift_0[16];
	uint8_t shift_1[16];
	uint8_t shift_2[16];
	uint8_t shift_3[16];
	uint8_t shift_4[16];
	uint8_t mix_0[16];
	uint8_t mix_1[16];
	uint8_t mix_2[16];
	uint8_t mix_3[16];
	uint8_t round_0[16];
	uint8_t round_1[16];
	uint8_t round_2[16];
	uint8_t round_3[16];
	uint8_t round_4[16];
	//#pragma HLS DATAFLOW



	addroundkey(state,0,sub_0,ekey);

	//addroundkey(state,0,cipher,ekey,start);

	for (loop1 =1;loop1 < nr; loop1++)
	{
		subbytes(sub_0,shift_0);
		shift_row_enc(shift_0,mix_0);
		mixcolumn(mix_0,round_0);
		addroundkey(round_0,loop1,sub_0,ekey);
	}


/*
	for (loop2 =nr/2;loop2 < nr; loop2++)
	{
		subbytes(sub_0,shift_1);
		shift_row_enc2(shift_1,mix_1);
		mixcolumn2(mix_1,round_1);
		addroundkey3(round_1,loop2,sub_0,ekey);
	}
/*
	for (loop3 =nr/2;loop3 < nr; loop3++)
	{
		subbytes4(sub_1,shift_2);
		shift_row_enc3(shift_2,mix_2);
		mixcolumn3(mix_2,round_2);
		addroundkey4(round_2,loop3,sub_1,ekey);
	}
*/
/*	subbytes2(sub_1,shift_1);
	shift_row_enc2(shift_1,mix_1);
	mixcolumn2(mix_1,round_1);
	addroundkey3(round_1,2,sub_2,ekey);
*/
	subbytes(sub_0,shift_0);
	shift_row_enc(shift_0,round_0);
	addroundkey(round_0,nr,cipher,ekey);

}


void shift_row_enc(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS PIPELINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	result[0] = state[0];
	result[1] = state[1];
	result[2] = state[2];
	result[3] = state[3];

	result[4] = state[5];
	result[5] = state[6];
	result[6] = state[7];
	result[7] = state[4];

	result[8] = state[10];
	result[10] = state[8];
	result[9] = state[11];
	result[11] = state[9];

	result[12] = state[15];
	result[15] = state[14];
	result[14] = state[13];
	result[13] = state[12];
}


void shift_row_enc2(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS PIPELINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	result[0] = state[0];
	result[1] = state[1];
	result[2] = state[2];
	result[3] = state[3];

	result[4] = state[5];
	result[5] = state[6];
	result[6] = state[7];
	result[7] = state[4];

	result[8] = state[10];
	result[10] = state[8];
	result[9] = state[11];
	result[11] = state[9];

	result[12] = state[15];
	result[15] = state[14];
	result[14] = state[13];
	result[13] = state[12];
}


void shift_row_enc3(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS PIPELINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	result[0] = state[0];
	result[1] = state[1];
	result[2] = state[2];
	result[3] = state[3];

	result[4] = state[5];
	result[5] = state[6];
	result[6] = state[7];
	result[7] = state[4];

	result[8] = state[10];
	result[10] = state[8];
	result[9] = state[11];
	result[11] = state[9];

	result[12] = state[15];
	result[15] = state[14];
	result[14] = state[13];
	result[13] = state[12];
}


void shift_row_enc4(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS PIPELINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	result[0] = state[0];
	result[1] = state[1];
	result[2] = state[2];
	result[3] = state[3];

	result[4] = state[5];
	result[5] = state[6];
	result[6] = state[7];
	result[7] = state[4];

	result[8] = state[10];
	result[10] = state[8];
	result[9] = state[11];
	result[11] = state[9];

	result[12] = state[15];
	result[15] = state[14];
	result[14] = state[13];
	result[13] = state[12];
}


void shift_row_enc5(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS PIPELINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	result[0] = state[0];
	result[1] = state[1];
	result[2] = state[2];
	result[3] = state[3];

	result[4] = state[5];
	result[5] = state[6];
	result[6] = state[7];
	result[7] = state[4];

	result[8] = state[10];
	result[10] = state[8];
	result[9] = state[11];
	result[11] = state[9];

	result[12] = state[15];
	result[15] = state[14];
	result[14] = state[13];
	result[13] = state[12];
}


void shift_row_enc6(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS PIPELINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	result[0] = state[0];
	result[1] = state[1];
	result[2] = state[2];
	result[3] = state[3];

	result[4] = state[5];
	result[5] = state[6];
	result[6] = state[7];
	result[7] = state[4];

	result[8] = state[10];
	result[10] = state[8];
	result[9] = state[11];
	result[11] = state[9];

	result[12] = state[15];
	result[15] = state[14];
	result[14] = state[13];
	result[13] = state[12];
}

void subbytes(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete
#pragma HLS array_partition variable=sbox complete
	uint8_t x, y; //addresses the matrix
#pragma HLS PIPELINE
	loop_sb1 : for(x=0;x<4;x++){
		loop_sb2 : for(y=0;y<4;y++){
			result[x*4+y] = sbox[state[x*4+y]];
		}//end y loop
	}//end x loop
}


void subbytes2(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=sbox complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	uint8_t x, y; //addresses the matrix
	loop_sb1 : for(x=0;x<4;x++){
#pragma HLS PIPELINE
		loop_sb2 : for(y=0;y<4;y++){
			result[x*4+y] = sbox[state[x*4+y]];
		}//end y loop
	}//end x loop
}


void subbytes3(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete
#pragma HLS array_partition variable=sbox complete
	uint8_t x, y; //addresses the matrix
	loop_sb1 : for(x=0;x<4;x++){
#pragma HLS PIPELINE
		loop_sb2 : for(y=0;y<4;y++){
			result[x*4+y] = sbox[state[x*4+y]];
		}//end y loop
	}//end x loop
}


void subbytes4(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=sbox complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	uint8_t x, y; //addresses the matrix
	loop_sb1 : for(x=0;x<4;x++){
#pragma HLS PIPELINE
		loop_sb2 : for(y=0;y<4;y++){
			result[x*4+y] = sbox[state[x*4+y]];
		}//end y loop
	}//end x loop
}


void subbytes5(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete
#pragma HLS array_partition variable=sbox complete
	uint8_t x, y; //addresses the matrix
	loop_sb1 : for(x=0;x<4;x++){
#pragma HLS PIPELINE
		loop_sb2 : for(y=0;y<4;y++){
			result[x*4+y] = sbox[state[x*4+y]];
		}//end y loop
	}//end x loop
}


void subbytes6(uint8_t state[16], uint8_t result[16])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=sbox complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	uint8_t x, y; //addresses the matrix
	loop_sb1 : for(x=0;x<4;x++){
#pragma HLS PIPELINE
		loop_sb2 : for(y=0;y<4;y++){
			result[x*4+y] = sbox[state[x*4+y]];
		}//end y loop
	}//end x loop
}



void mixcolumn(uint8_t state[16],uint8_t result[16])
{
	//B1� = (B1 * 2) XOR (B2 * 3) XOR (B3 * 1) XOR (B4 * 1)
	//B2� = (B1 * 1) XOR (B2 * 2) XOR (B3 * 3) XOR (B4 * 1)
	//B3� = (B1 * 1) XOR (B2 * 1) XOR (B3 * 2) XOR (B4 * 3)
	//B4� = (B1 * 3) XOR (B2 * 1) XOR (B3 * 1) XOR (B4 * 2)

	uint8_t x; // control of the column
//#pragma HLS INLINE
#pragma HLS array_partition variable=gf3 complete
#pragma HLS array_partition variable=gf2 complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

//#pragma HLS PIPELINE
	loop_mx1 :for(x=0;x<4;x++){
		#pragma HLS PIPELINE
		result[x] = (gf2[state[x]])^(gf3[state[4+x]])^(state[8+x])^(state[12+x]);
		result[4+x] = (state[x])^(gf2[state[4+x]])^(gf3[state[8+x]])^(state[12+x]);
		result[8+x] = (state[x])^(state[4+x])^(gf2[state[8+x]])^(gf3[state[12+x]]);
		result[12+x] = (gf3[state[x]])^(state[4+x])^(state[8+x])^(gf2[state[12+x]]);
	}
}


void mixcolumn2(uint8_t state[16],uint8_t result[16])
{
	//B1� = (B1 * 2) XOR (B2 * 3) XOR (B3 * 1) XOR (B4 * 1)
	//B2� = (B1 * 1) XOR (B2 * 2) XOR (B3 * 3) XOR (B4 * 1)
	//B3� = (B1 * 1) XOR (B2 * 1) XOR (B3 * 2) XOR (B4 * 3)
	//B4� = (B1 * 3) XOR (B2 * 1) XOR (B3 * 1) XOR (B4 * 2)

	uint8_t x; // control of the column
//#pragma HLS INLINE
#pragma HLS array_partition variable=gf3 complete
#pragma HLS array_partition variable=gf2 complete

	loop_mx1 :for(x=0;x<4;x++){
		#pragma HLS PIPELINE
		result[x] = (gf2[state[x]])^(gf3[state[4+x]])^(state[8+x])^(state[12+x]);
		result[4+x] = (state[x])^(gf2[state[4+x]])^(gf3[state[8+x]])^(state[12+x]);
		result[8+x] = (state[x])^(state[4+x])^(gf2[state[8+x]])^(gf3[state[12+x]]);
		result[12+x] = (gf3[state[x]])^(state[4+x])^(state[8+x])^(gf2[state[12+x]]);
	}
}


void mixcolumn3(uint8_t state[16],uint8_t result[16])
{
	//B1� = (B1 * 2) XOR (B2 * 3) XOR (B3 * 1) XOR (B4 * 1)
	//B2� = (B1 * 1) XOR (B2 * 2) XOR (B3 * 3) XOR (B4 * 1)
	//B3� = (B1 * 1) XOR (B2 * 1) XOR (B3 * 2) XOR (B4 * 3)
	//B4� = (B1 * 3) XOR (B2 * 1) XOR (B3 * 1) XOR (B4 * 2)

	uint8_t x; // control of the column
//#pragma HLS INLINE
#pragma HLS array_partition variable=gf3 complete
#pragma HLS array_partition variable=gf2 complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	loop_mx1 :for(x=0;x<4;x++){
		#pragma HLS PIPELINE
		result[x] = (gf2[state[x]])^(gf3[state[4+x]])^(state[8+x])^(state[12+x]);
		result[4+x] = (state[x])^(gf2[state[4+x]])^(gf3[state[8+x]])^(state[12+x]);
		result[8+x] = (state[x])^(state[4+x])^(gf2[state[8+x]])^(gf3[state[12+x]]);
		result[12+x] = (gf3[state[x]])^(state[4+x])^(state[8+x])^(gf2[state[12+x]]);
	}
}


void mixcolumn4(uint8_t state[16],uint8_t result[16],bool end)
{
	//B1� = (B1 * 2) XOR (B2 * 3) XOR (B3 * 1) XOR (B4 * 1)
	//B2� = (B1 * 1) XOR (B2 * 2) XOR (B3 * 3) XOR (B4 * 1)
	//B3� = (B1 * 1) XOR (B2 * 1) XOR (B3 * 2) XOR (B4 * 3)
	//B4� = (B1 * 3) XOR (B2 * 1) XOR (B3 * 1) XOR (B4 * 2)

	uint8_t x; // control of the column
//#pragma HLS INLINE
#pragma HLS array_partition variable=gf3 complete
#pragma HLS array_partition variable=gf2 complete

	loop_mx1 :for(x=0;x<4;x++){
		#pragma HLS PIPELINE
		if (end == 0)
		{
			result[x] = (gf2[state[x]])^(gf3[state[4+x]])^(state[8+x])^(state[12+x]);
			result[4+x] = (state[x])^(gf2[state[4+x]])^(gf3[state[8+x]])^(state[12+x]);
			result[8+x] = (state[x])^(state[4+x])^(gf2[state[8+x]])^(gf3[state[12+x]]);
			result[12+x] = (gf3[state[x]])^(state[4+x])^(state[8+x])^(gf2[state[12+x]]);
		}
		else
		{
			result[x] = state[x];
			result[4+x] = state[4+x];
			result[8+x] = state[8+x];
			result[12+x] = state[12+x];
		}
	}
}


void mixcolumn5(uint8_t state[16],uint8_t result[16])
{
	//B1� = (B1 * 2) XOR (B2 * 3) XOR (B3 * 1) XOR (B4 * 1)
	//B2� = (B1 * 1) XOR (B2 * 2) XOR (B3 * 3) XOR (B4 * 1)
	//B3� = (B1 * 1) XOR (B2 * 1) XOR (B3 * 2) XOR (B4 * 3)
	//B4� = (B1 * 3) XOR (B2 * 1) XOR (B3 * 1) XOR (B4 * 2)

	uint8_t x; // control of the column
//#pragma HLS INLINE
#pragma HLS array_partition variable=gf3 complete
#pragma HLS array_partition variable=gf2 complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	loop_mx1 :for(x=0;x<4;x++){
		#pragma HLS PIPELINE
		result[x] = (gf2[state[x]])^(gf3[state[4+x]])^(state[8+x])^(state[12+x]);
		result[4+x] = (state[x])^(gf2[state[4+x]])^(gf3[state[8+x]])^(state[12+x]);
		result[8+x] = (state[x])^(state[4+x])^(gf2[state[8+x]])^(gf3[state[12+x]]);
		result[12+x] = (gf3[state[x]])^(state[4+x])^(state[8+x])^(gf2[state[12+x]]);
	}
}


void mixcolumn6(uint8_t state[16],uint8_t result[16])
{
	//B1� = (B1 * 2) XOR (B2 * 3) XOR (B3 * 1) XOR (B4 * 1)
	//B2� = (B1 * 1) XOR (B2 * 2) XOR (B3 * 3) XOR (B4 * 1)
	//B3� = (B1 * 1) XOR (B2 * 1) XOR (B3 * 2) XOR (B4 * 3)
	//B4� = (B1 * 3) XOR (B2 * 1) XOR (B3 * 1) XOR (B4 * 2)

	uint8_t x; // control of the column
//#pragma HLS INLINE
#pragma HLS array_partition variable=gf3 complete
#pragma HLS array_partition variable=gf2 complete

	loop_mx1 :for(x=0;x<4;x++){
		#pragma HLS PIPELINE
		result[x] = (gf2[state[x]])^(gf3[state[4+x]])^(state[8+x])^(state[12+x]);
		result[4+x] = (state[x])^(gf2[state[4+x]])^(gf3[state[8+x]])^(state[12+x]);
		result[8+x] = (state[x])^(state[4+x])^(gf2[state[8+x]])^(gf3[state[12+x]]);
		result[12+x] = (gf3[state[x]])^(state[4+x])^(state[8+x])^(gf2[state[12+x]]);
	}
}

void addroundkey(uint8_t state[16], uint8_t iteration,uint8_t result[16],uint8_t ekey[240])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=ekey complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	  uint8_t x,y;

/*
	  loop_fast:for(x=0;x<16;x++) {
  #pragma HLS PIPELINE
//#pragma HLS UNROLL
			  //if (start == 1)
				  result [x] = state [x] ^ ekey[iteration * nb * 4 + x * nb + x];
			  //else
			  //	  result [y+x*4] = state [y+x*4];
	    }
	  }*/
#pragma HLS PIPELINE
  loop_rk1 :for(x=0;x<4;x++) {
   //#pragma HLS PIPELINE
		  loop_rk2 : for(y=0;y<4;y++){
				  result [y+x*4] = state [y+x*4] ^ ekey[iteration * nb * 4 + x * nb + y];
	    }
	  }
}

void addroundkey2(uint8_t state[16], uint8_t iteration,uint8_t result[16],uint8_t ekey[240])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete
#pragma HLS array_partition variable=ekey complete
	  uint8_t x,y;
	  loop_rk1 :for(x=0;x<4;x++) {
#pragma HLS PIPELINE
		  loop_rk2 : for(y=0;y<4;y++){
			  result [y+x*4] = state [y+x*4] ^ ekey[iteration * nb * 4 + x * nb + y];
	    }
	  }
}


void addroundkey3(uint8_t state[16], uint8_t iteration,uint8_t result[16],uint8_t ekey[240])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=ekey complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	  uint8_t x,y;
	  loop_rk1 :for(x=0;x<4;x++) {
#pragma HLS PIPELINE
		  loop_rk2 : for(y=0;y<4;y++){
			  result [y+x*4] = state [y+x*4] ^ ekey[iteration * nb * 4 + x * nb + y];
	    }
	  }
}

void addroundkey4(uint8_t state[16], uint8_t iteration,uint8_t result[16],uint8_t ekey[240])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=ekey complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	  uint8_t x,y;
	  loop_rk1 :for(x=0;x<4;x++) {
#pragma HLS PIPELINE
		  loop_rk2 : for(y=0;y<4;y++){
			  result [y+x*4] = state [y+x*4] ^ ekey[iteration * nb * 4 + x * nb + y];
	    }
	  }
}

void addroundkey5(uint8_t state[16], uint8_t iteration,uint8_t result[16],uint8_t ekey[240])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=ekey complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	  uint8_t x,y;
	  loop_rk1 :for(x=0;x<4;x++) {
#pragma HLS PIPELINE
		  loop_rk2 : for(y=0;y<4;y++){
			  result [y+x*4] = state [y+x*4] ^ ekey[iteration * nb * 4 + x * nb + y];
	    }
	  }
}

void addroundkey6(uint8_t state[16], uint8_t iteration,uint8_t result[16],uint8_t ekey[240])
{
//#pragma HLS INLINE
#pragma HLS array_partition variable=ekey complete
#pragma HLS array_partition variable=state complete
#pragma HLS array_partition variable=result complete

	  uint8_t x,y;
	  loop_rk1 :for(x=0;x<4;x++) {
#pragma HLS PIPELINE
		  loop_rk2 : for(y=0;y<4;y++){
			  result [y+x*4] = state [y+x*4] ^ ekey[iteration * nb * 4 + x * nb + y];
	    }
	  }
}

